using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTimeUI : MonoBehaviour
{
    [SerializeField] TMPro.TMP_Text maxTimeText;
    [SerializeField] TMPro.TMP_Text currentTimeText;
    private FlappyBirdGameManager gameManager;

    private void Awake()
    {
        gameManager = FindObjectOfType<FlappyBirdGameManager>();
    }

    private void Update()
    {
        maxTimeText.text = $"Max: {(int)gameManager.MaxTimer}";
        currentTimeText.text = $"Time: {(int)gameManager.Timer}";
    }
}
