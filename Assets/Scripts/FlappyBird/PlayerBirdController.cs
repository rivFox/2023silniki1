using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class PlayerBirdController : MonoBehaviour
{
    private Rigidbody2D rb;
    [SerializeField] private float strength;
    [SerializeField] private UnityEvent onJump;
    
    private FlappyBirdGameManager gameManager;

    private void Awake()
    {
        gameManager = FindObjectOfType<FlappyBirdGameManager>();
        rb = GetComponent<Rigidbody2D>();

        onJump.AddListener(() =>
        {
            GetComponentInChildren<ParticleSystem>().Play();
        });
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            rb.velocity = Vector2.up * strength;
            onJump.Invoke();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.LogError("Game Over", collision.collider.gameObject);
        gameManager.GameOver();
    }
}
