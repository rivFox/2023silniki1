using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipesManager : MonoBehaviour
{
    [Header("PipeSettings")]
    [SerializeField] PipeController pipePrefab;
    [SerializeField] float pipeSpeed = 1f;
    [SerializeField] float spawnHeight = 1f;

    [Header("SpawnSettings")]
    [SerializeField] float spawnRate = 1f;
    [SerializeField] Transform spawnPositionTransform;

    [Header("OtherSettings")]
    [SerializeField] float destroyTime = 10f;

    float timer;

    private void Awake()
    {
        timer = spawnRate;
    }

    private void Update()
    {
        timer += Time.deltaTime;

        if(timer > spawnRate)
        {
            timer = 0f;
            SpawnPipe();
        }
    }

    private void SpawnPipe()
    {
        var position = new Vector3(spawnPositionTransform.position.x, Random.Range(-spawnHeight, spawnHeight), 0f);
        var pipe = Instantiate(pipePrefab, position, Quaternion.identity, transform);
        pipe.Speed = pipeSpeed;
        Destroy(pipe.gameObject, destroyTime);
    }
}
